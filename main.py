from matplotlib import pyplot as plt
import numpy as np
import math

landmarks = np.array([[3.5, 2], [3.5, -2], [0, -4]])
# Die Orthogonalen Vektoren.


# Die Methode ist allgemeingehalten somit kann man
# einen beliebigen ursprung(Origin) und beliebige Landmarks wählen
def originToLandmark(origin, landmarks):
    arr = []
    for i in range(len(landmarks)):
        temp = -origin + landmarks[i]
        arr.append(temp)
    return arr


def VektorBetrag(landmarksOrthogonal):

    einheitsVektorL = []
    einheitsVektorL.append(math.sqrt(landmarksOrthogonal[0] ** 2 + landmarksOrthogonal[1] ** 2))
    return einheitsVektorL


def EinsDurchVektorBetrag(einheitsVektoren):
    temp = []
    for i in range(len(einheitsVektoren)):
        temp.append(1 / einheitsVektoren[i])
    return temp


def VektorBetragMalKehrwert(einheitsVektorDurch1, landmarksInverse):
    temp = (einheitsVektorDurch1 * landmarksInverse)
    return temp


def RadiusLandmarkMalVektor(landmarkInverse):
    vektorenZumLandmak = (landmarkInverse * 0.5)
    return vektorenZumLandmak


def Aufruf(SP):
    tempBetrag = VektorBetrag(SP)
    tempBetrag = EinsDurchVektorBetrag(tempBetrag)
    temp = np.array(VektorBetragMalKehrwert(tempBetrag, SP))
    RLV = np.array(RadiusLandmarkMalVektor(temp))
    return RLV


def VektorenZuLandmarkKreisLinks(landmarkOriginSnap, KegelVektorenLinksRechts):
    temp = []
    for i in range(len(landmarkOriginSnap)):
        temp.append(landmarkOriginSnap[i] + KegelVektorenLinksRechts[i])
    return temp


def VektorenZuLandmarkKreisRechts(landmarkOriginSnap, KegelVektorenLinksRechts):
    temp = []
    for i in range(len(landmarkOriginSnap)):
        temp.append(landmarkOriginSnap[i] - KegelVektorenLinksRechts[i])
    return temp


def VektorenZwischenZweiKegelSnapshot(VektorenZuLandmarkKreisLinks, VektorenZuLandmarkKreisRechts):
    temp = []
    v1 = VektorenZuLandmarkKreisRechts[0] + VektorenZuLandmarkKreisLinks[1]
    v2 = VektorenZuLandmarkKreisLinks[0] + VektorenZuLandmarkKreisRechts[2]
    temp.append(v1)
    temp.append(v2)
    return temp


def WinkelDerVektoren(LandmarkOriginSnapshot, LandmarkOriginRetina):
    winkel = []
    for i in range(len(LandmarkOriginRetina)):
        unit_vector_1 = LandmarkOriginSnapshot[i] / np.linalg.norm(LandmarkOriginSnapshot[i])
        unit_vector_2 = LandmarkOriginRetina[i] / np.linalg.norm(LandmarkOriginRetina[i])
        dot_product = np.dot(unit_vector_1, unit_vector_2)
        angle = np.arccos(dot_product)
        winkel.append(math.degrees(angle))
    return winkel


def WinkelTemp(LandmarkOriginSnapshot, LandmarkOriginRetina):
    unit_vector_1 = LandmarkOriginSnapshot / np.linalg.norm(LandmarkOriginSnapshot)
    unit_vector_2 = LandmarkOriginRetina / np.linalg.norm(LandmarkOriginRetina)
    dot_product = np.dot(unit_vector_1, unit_vector_2)
    angle = np.arccos(dot_product)
    return math.degrees(angle)


def WinkelIndex(LandmarkOriginSnapshot, LandmarkOriginRetina):
    winkel = []
    for i in range(len(LandmarkOriginRetina)):
        unit_vector_1 = LandmarkOriginSnapshot / np.linalg.norm(LandmarkOriginSnapshot)
        unit_vector_2 = LandmarkOriginRetina[i] / np.linalg.norm(LandmarkOriginRetina[i])
        dot_product = np.dot(unit_vector_1, unit_vector_2)
        angle = np.arccos(dot_product)
        winkel.append(math.degrees(angle))
    return winkel


def LinksODerRechtsTurningVektor(LandmarkOriginSnapshot, landmarkOriginRetina):
    TunrinVektorLoderR = landmarkOriginRetina[1] * LandmarkOriginSnapshot[0] - landmarkOriginRetina[0] *LandmarkOriginSnapshot[1]
    return TunrinVektorLoderR


def kleinsterWinkel(winkel):
    temp = []
    temp.append(np.where(np.amin(winkel)))
    temp.append(np.amin(winkel))
    return temp


def TurningVektor(winkelSnapshot, winkelRetina, SnapshotVektor, RetinaVektor):
    temp = []
    if LinksODerRechtsTurningVektor(SnapshotVektor, RetinaVektor) > 0:
        if winkelRetina < winkelSnapshot:
            temp = np.array([RetinaVektor[1], -RetinaVektor[0]])
        elif winkelRetina > winkelSnapshot:
            temp = np.array([-RetinaVektor[1], RetinaVektor[0]])
        else:
            return np.array([0.0,0.0])
    elif LinksODerRechtsTurningVektor(SnapshotVektor, RetinaVektor) < 0:
        if winkelRetina < winkelSnapshot:
            temp = np.array([-RetinaVektor[1], RetinaVektor[0]])
        elif winkelRetina > winkelSnapshot:
            temp = np.array([RetinaVektor[1], -RetinaVektor[0]])
        else: 
            return np.array([0.0,0.0])
    else:
        return np.array([0.0, 0.0])
    tempBetrag = VektorBetrag(temp)
    tempBetrag = EinsDurchVektorBetrag(tempBetrag)
    temp = VektorBetragMalKehrwert(tempBetrag, temp)
    return temp


def ApprocheVektor(winkelSnapshot, winkelRetina, RetinaVektor):

    if winkelRetina < winkelSnapshot:
        temp = -RetinaVektor
    elif winkelRetina > winkelSnapshot:
        temp = RetinaVektor
    else:
        return np.array([0.0,0.0])
    tempBetrag = VektorBetrag(temp)
    tempBetrag = EinsDurchVektorBetrag(tempBetrag)
    temp = VektorBetragMalKehrwert(tempBetrag, temp)

    return temp
