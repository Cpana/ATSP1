import numpy as np
import main
import matplotlib.pyplot as plt

vektorarr = []
home = 112


class Positionen():

    def __init__(self, vektoren, vektorenLandmarks, Winkel, vektorenZwischenLandmarks, kegelVektorenLinks,
                 kegelVektorRechts, winkelZwischenLandmarks,vektorIndex, zwischenIndex,homingVektor,TurningVektor, ApprochVektor ):
        self.vektoren = vektoren
        self.vektorenLandmarks = vektorenLandmarks
        self.Winkel = Winkel
        self.vektorenZwischenLandmarks = vektorenZwischenLandmarks
        self.kegelVektorenLinks = kegelVektorenLinks
        self.kegelVektorenRechts = kegelVektorRechts
        self.winkelZwischenLandmarks = winkelZwischenLandmarks
        self.homingVektor = homingVektor
        self.vektorIndex = vektorIndex
        self.kleinserWinkel = vektorIndex
        self.zwischenIndex = zwischenIndex
        self.TurningVektor = TurningVektor
        self.ApprochVektor = ApprochVektor



for i in range(225):
    vektorarr.append(Positionen(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))



#Vekotren von -7,-7 bis 7,7
#Ortsvektor
x = 0
for j in range(-7, 8):
    for u in range(-7, 8):
        vektorarr[x].vektoren = np.array([j, u])
        x = x + 1

#Vekotren zum Landmark
x = 0
for j in range(-7, 8):
    for u in range(-7, 8):
        vektorarr[x].vektorenLandmarks = main.originToLandmark(vektorarr[x].vektoren, main.landmarks)
        x = x + 1



#Kegel Vekotren
x = 0
for j in range(-7, 8):

    for u in range(-7, 8):
        tempRR= []
        tempLL= []
        if x == 108:
            x = x + 1
            continue
        else :

            for t in range(3):
                temp = np.array(main.Aufruf(vektorarr[x].vektorenLandmarks[t]))
                tempL = np.array([-temp[1], temp[0]])
                tempR = np.array([temp[1], -temp[0]])
                tempRR.append(tempR)
                tempLL.append(tempL)
            tempLL = np.array(tempLL)
            tempRR = np.array(tempRR)
            vektorarr[x].kegelVektorenLinks = vektorarr[x].vektorenLandmarks + tempLL
            vektorarr[x].kegelVektorenRechts= vektorarr[x].vektorenLandmarks + tempRR
            x = x+1






#Kegelwinkel bestimmen
x = 0
for j in range(-7, 8):
    for u in range(-7, 8):
        if x == 108:
            x = x + 1
            continue
        else:
            vektorarr[x].Winkel = np.array(main.WinkelDerVektoren(vektorarr[x].kegelVektorenLinks, vektorarr[x].kegelVektorenRechts))
            x = x+1


#Bestimmen der Vektoren/Winkel die zwischen den Landmarksvektoren
x = 0
for j in range(-7, 8):
    for u in range(-7, 8):
            tempList = []
            tempListWinkel = []
            if x == 108:
                x = x + 1
                continue
            temp = vektorarr[x].kegelVektorenLinks[1] + vektorarr[x].kegelVektorenRechts[0]
            winkel1 = main.WinkelTemp(vektorarr[x].kegelVektorenLinks[1],vektorarr[x].kegelVektorenRechts[0])
            tempListWinkel.append(winkel1)
            tempList.append(temp)

            temp2 = vektorarr[x].kegelVektorenLinks[2] + vektorarr[x].kegelVektorenRechts[1]
            winkel2 = main.WinkelTemp(vektorarr[x].kegelVektorenLinks[2] , vektorarr[x].kegelVektorenRechts[1])
            tempListWinkel.append(winkel2)
            tempList.append(temp2)

            temp3 = -(vektorarr[x].kegelVektorenLinks[0] + vektorarr[x].kegelVektorenRechts[2])
            winkel3 = main.WinkelTemp(vektorarr[x].kegelVektorenLinks[0] , vektorarr[x].kegelVektorenRechts[2])
            tempListWinkel.append(winkel3)
            tempList.append(temp3)

            vektorarr[x].winkelZwischenLandmarks = tempListWinkel
            vektorarr[x].vektorenZwischenLandmarks = tempList
            x = x + 1


#Index zum nächsten 
x = 0
for j in range(-7, 8):
    for u in range(-7, 8):
        if x == 108:
            x = x + 1
            continue
        else:
            winkelListeKleinster = []
            vektorZu0 = np.array([vektorarr[112].vektorenLandmarks[0]])
            vektorZu1 = np.array([vektorarr[112].vektorenLandmarks[1]])
            vektorZu2 = np.array([vektorarr[112].vektorenLandmarks[2]])


            winkelListe = np.array(main.WinkelIndex(vektorZu0 ,np.array(vektorarr[x].vektorenLandmarks)))
            winkelListe1 = np.array(main.WinkelIndex(vektorZu1 ,np.array(vektorarr[x].vektorenLandmarks)))
            winkelListe2 = np.array(main.WinkelIndex(vektorZu2 ,np.array(vektorarr[x].vektorenLandmarks)))

            winkelListeKleinster.append(np.argmin(winkelListe))
            winkelListeKleinster.append(np.argmin(winkelListe1))
            winkelListeKleinster.append(np.argmin(winkelListe2))
            vektorarr[x].vektorIndex = winkelListeKleinster

            x =  x+1


#
x = 0
for j in range(-7, 8):
    for u in range(-7, 8):
        if x == 108:
            x = x + 1
            continue
        else:
            winkelListeKleinsterZwischen = []
            vektorZwischen0 = np.array([vektorarr[112].vektorenZwischenLandmarks[0]])
            vektorZwischen1 = np.array([vektorarr[112].vektorenZwischenLandmarks[1]])
            vektorZwischen2 = np.array([vektorarr[112].vektorenZwischenLandmarks[2]])


            winkelListeZwischen = np.array(main.WinkelIndex(vektorZwischen0, np.array(vektorarr[x].vektorenZwischenLandmarks)))
            winkelListe1Zwischen = np.array(main.WinkelIndex(vektorZwischen1, np.array(vektorarr[x].vektorenZwischenLandmarks)))
            winkelListe2Zwischen = np.array(main.WinkelIndex(vektorZwischen2, np.array(vektorarr[x].vektorenZwischenLandmarks)))

            winkelListeKleinsterZwischen.append(np.argmin(winkelListeZwischen))
            winkelListeKleinsterZwischen.append(np.argmin(winkelListe1Zwischen))
            winkelListeKleinsterZwischen.append(np.argmin(winkelListe2Zwischen))
            vektorarr[x].zwischenIndex = winkelListeKleinsterZwischen

            x = x+1


#Berechnung TurningVektor
x = 0
for j in range(-7, 8):
    for u in range(-7, 8):
        if x == 108:
            x = x + 1
            continue
        else:
            tempTunringVektor = []
            tempTunringVektor.append(main.TurningVektor(vektorarr[112].Winkel[0],
                                                        vektorarr[x].Winkel[vektorarr[x].vektorIndex[0]],vektorarr[112].vektorenLandmarks[0],vektorarr[x].vektorenLandmarks[vektorarr[x].vektorIndex[0]]))
            tempTunringVektor.append(main.TurningVektor(vektorarr[112].Winkel[1],
                                                        vektorarr[x].Winkel[vektorarr[x].vektorIndex[1]],vektorarr[112].vektorenLandmarks[1],vektorarr[x].vektorenLandmarks[vektorarr[x].vektorIndex[1]]))
            tempTunringVektor.append(main.TurningVektor(vektorarr[112].Winkel[2],
                                                        vektorarr[x].Winkel[vektorarr[x].vektorIndex[2]],vektorarr[112].vektorenLandmarks[2],vektorarr[x].vektorenLandmarks[vektorarr[x].vektorIndex[2]]))

            tempTunringVektor.append(main.TurningVektor(vektorarr[112].winkelZwischenLandmarks[0],
                                                        vektorarr[x].winkelZwischenLandmarks[vektorarr[x].zwischenIndex[0]],
                                                        vektorarr[112].vektorenZwischenLandmarks[0],
                                                        vektorarr[x].vektorenZwischenLandmarks[vektorarr[x].zwischenIndex[0]]))
                                                        
            tempTunringVektor.append(main.TurningVektor(vektorarr[112].winkelZwischenLandmarks[1],
                                                        vektorarr[x].winkelZwischenLandmarks[vektorarr[x].zwischenIndex[1]],
                                                        vektorarr[112].vektorenZwischenLandmarks[1],
                                                        vektorarr[x].vektorenZwischenLandmarks[vektorarr[x].zwischenIndex[1]]))

            tempTunringVektor.append(main.TurningVektor(vektorarr[112].winkelZwischenLandmarks[2],
                                                        vektorarr[x].winkelZwischenLandmarks[vektorarr[x].zwischenIndex[2]],
                                                        vektorarr[112].vektorenZwischenLandmarks[2],
                                                        vektorarr[x].vektorenZwischenLandmarks[vektorarr[x].zwischenIndex[2]]))

            TurningKomplett = 0.0

            for i in range(6):
                TurningKomplett += tempTunringVektor[i]
            vektorarr[x].TurningVektor = TurningKomplett
            x = x+1


#Berechnung ApproachVektor
x = 0
for j in range(-7, 8):
    for u in range(-7, 8):
        if x == 108:
            x = x + 1
            continue
        else:
                tempApprochVektor = []
                tempApprochVektor.append(main.ApprocheVektor(vektorarr[112].Winkel[0],
                                                            vektorarr[x].Winkel[vektorarr[x].vektorIndex[0]],
                                                            vektorarr[x].vektorenLandmarks[
                                                            vektorarr[x].vektorIndex[0]]))

                tempApprochVektor.append(main.ApprocheVektor(vektorarr[112].Winkel[1],
                                                            vektorarr[x].Winkel[vektorarr[x].vektorIndex[1]],
                                                            vektorarr[x].vektorenLandmarks[
                                                            vektorarr[x].vektorIndex[1]]))

                tempApprochVektor.append(main.ApprocheVektor(vektorarr[112].Winkel[2],
                                                            vektorarr[x].Winkel[vektorarr[x].vektorIndex[2]],
                                                            vektorarr[x].vektorenLandmarks[
                                                            vektorarr[x].vektorIndex[2]]))

                tempApprochVektor.append(main.ApprocheVektor(vektorarr[112].winkelZwischenLandmarks[0],
                                                            vektorarr[x].winkelZwischenLandmarks[vektorarr[x].zwischenIndex[0]],
                                                            vektorarr[x].vektorenZwischenLandmarks[vektorarr[x].zwischenIndex[0]]))

                tempApprochVektor.append(main.ApprocheVektor(vektorarr[112].winkelZwischenLandmarks[1],
                                                            vektorarr[x].winkelZwischenLandmarks[vektorarr[x].zwischenIndex[1]],
                                                            vektorarr[x].vektorenZwischenLandmarks[vektorarr[x].zwischenIndex[1]]))

                tempApprochVektor.append(main.ApprocheVektor(vektorarr[112].winkelZwischenLandmarks[2],
                                                            vektorarr[x].winkelZwischenLandmarks[vektorarr[x].zwischenIndex[2]],
                                                            vektorarr[x].vektorenZwischenLandmarks[vektorarr[x].zwischenIndex[2]]))

                ApprocheKomplett = 0.0

                for i in range(6):
                    ApprocheKomplett += tempApprochVektor[i]
                Approch = ApprocheKomplett  * 3
                vektorarr[x].ApprochVektor = Approch
                x = x + 1

#Berechnung Homing Vektor
x = 0
for j in range(-7, 8):
    for u in range(-7, 8):
        if x == 108 or  x == 112:
            x = x + 1
            continue
        else:
            AT = vektorarr[x].ApprochVektor + vektorarr[x].TurningVektor
            AT1 = main.VektorBetrag(AT)
            AT1 = main.EinsDurchVektorBetrag(AT1)
            vektorarr[x].homingVektor = main.VektorBetragMalKehrwert(AT1, -AT)
            x = x+1


#Bestimmung der Homing-Precision
homingprozent = 0.0
homingwinkel = 0.0
x = 0
for j in range(-7, 8):
    for u in range(-7, 8):
        if x == 108 or  x == 112:
            x = x + 1
            continue
        else:
            temp = main.WinkelTemp(vektorarr[x].homingVektor, -vektorarr[x].vektoren)
            temp2 = 180 - temp
            homingwinkel = homingwinkel + temp
            temp2 = temp2 / 180
            homingprozent = temp2 + homingprozent
            x = x + 1


#Plotten
homingwinkel = round(homingwinkel/225, 2)
homingprozent = round(homingprozent / 225 *100,2)
str1 = "Homing-precision:\n"+ str(homingwinkel)+ "°\n" + str(homingprozent) + "%"
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.spines.left.set_position('zero')
ax.spines.right.set_color('none')
ax.spines.bottom.set_position('zero')
ax.spines.top.set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')
ax.set_xlim(-7, 7)
ax.set_ylim(-7,7)
ax.set_title("Vektor zur Landmark Visualliserung, Red Dot ist der Ursprung")
for i in range(224):
    if i == 108 or i == 112:
        i = i+1
        continue
    else:
        plt.quiver(vektorarr[i].vektoren[0],vektorarr[i].vektoren[1] ,vektorarr[i].homingVektor[0],vektorarr[i].homingVektor[1],scale= 4, scale_units='xy', angles='xy')
plt.scatter(0,0, color='red')
plt.scatter(3.5,2, color= 'blue')
plt.scatter(3.5,-2, color= 'blue')
plt.scatter(0,-4, color= 'blue')
plt.title(str1)
plt.show()